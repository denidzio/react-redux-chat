import React, { ChangeEvent, Component, SyntheticEvent } from "react";
import { createRef } from "react";
import { connect, RootStateOrAny } from "react-redux";

import {
  IMessageInputProps,
  IMessageInputState,
} from "./interfaces/messageInput";
import { sendMessage } from "../actions/chat";

class MessageInput extends Component<IMessageInputProps, IMessageInputState> {
  inputRef = createRef<HTMLInputElement>();

  constructor(props: IMessageInputProps) {
    super(props);

    this.state = { message: "" };

    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInput(e: ChangeEvent<HTMLInputElement>) {
    this.setState({ message: e.currentTarget.value });
  }

  handleSubmit(e: SyntheticEvent) {
    e.preventDefault();

    if (!this.state.message.trim()) {
      return;
    }

    this.props.sendMessage(this.state.message);
    this.setState({ message: "" });
  }

  render() {
    return (
      <form className="message-input" onSubmit={this.handleSubmit}>
        <label htmlFor="message" className="message-input-label">
          <input
            ref={this.inputRef}
            onChange={this.handleInput}
            value={this.state.message}
            type="text"
            className="message-input-text"
            id="message"
            placeholder="Enter your message..."
            autoComplete="off"
          />
        </label>
        <button
          type="submit"
          className="message-input-button message-input__button"
        >
          Send
        </button>
      </form>
    );
  }
}

const mapStateToProps = (state: RootStateOrAny) => ({});

const mapDispatchToProps = {
  sendMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
