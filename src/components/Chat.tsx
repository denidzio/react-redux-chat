import React, { Component, Fragment } from "react";
import { connect, RootStateOrAny } from "react-redux";

import data from "../fakeData/messages.json";

import { IMessage } from "../interfaces";
import { IChatProps, IChatState } from "./interfaces/chat";
import { get } from "../api/common.api";

import { EditModal, Header, MessageInput, MessageList, Preloader } from "./";
import { hidePreloader, setMessages } from "../actions/chat/";

class Chat extends Component<IChatProps, IChatState> {
  async componentDidMount() {
    const messages = await get<IMessage[]>(this.props.url);

    if (messages) {
      this.props.setMessages(messages);
    }

    // this.props.setMessages(data);
    this.props.hidePreloader();
  }

  render() {
    if (this.props.isLoad) {
      return <Preloader />;
    }

    return (
      <Fragment>
        <div className="chat">
          <Header />
          <MessageList />
          <MessageInput />
        </div>
        <EditModal />
      </Fragment>
    );
  }
}

const mapStateToProps = (state: RootStateOrAny) => ({
  isLoad: state.chat.preloader,
});

const mapDispatchToProps = {
  hidePreloader,
  setMessages,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
