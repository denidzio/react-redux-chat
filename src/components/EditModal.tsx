import React, { ChangeEvent, Component, SyntheticEvent } from "react";
import { connect, RootStateOrAny } from "react-redux";

import { IEditModalProps, IEditModalState } from "./interfaces/editModal";
import { hideEditModal, editMessage, setEditedMessage } from "../actions/chat";
import { createRef } from "react";

class EditModal extends Component<IEditModalProps, IEditModalState> {
  modalWrapperRef = createRef<HTMLDivElement>();
  modalCloseBtnRef = createRef<HTMLDivElement>();
  inputRef = createRef<HTMLInputElement>();

  constructor(props: IEditModalProps) {
    super(props);

    this.state = {
      message: "",
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate(prevProps: IEditModalProps) {
    if (!prevProps.editedMessage && this.props.editedMessage) {
      this.setState({ message: this.props.editedMessage.text });
      this.inputRef.current?.focus();
    }
  }

  handleClose(e: SyntheticEvent) {
    const target = e.target;

    if (
      target === this.modalWrapperRef.current ||
      target === this.modalCloseBtnRef.current
    ) {
      this.props.hideEditModal();
      this.props.setEditedMessage(null);
    }
  }

  handleInput(e: ChangeEvent<HTMLInputElement>) {
    this.setState({ message: e.currentTarget.value });
  }

  handleSubmit(e: SyntheticEvent) {
    e.preventDefault();

    if (!this.state.message.trim()) {
      return;
    }

    this.props.editMessage(this.state.message);
    this.props.hideEditModal();
    this.props.setEditedMessage(null);
  }

  render() {
    return (
      <div
        className={`edit-message-modal ${
          this.props.isShown && this.props.editedMessage ? "modal-shown" : ""
        }`}
        ref={this.modalWrapperRef}
        onClick={this.handleClose}
      >
        <form className="edit-message-modal-form" onSubmit={this.handleSubmit}>
          <h2 className="edit-message-modal-form-title edit-message-modal-form__title">
            Edit message
          </h2>
          <div
            className="edit-message-close edit-message-modal-form__close close"
            ref={this.modalCloseBtnRef}
          ></div>
          <label htmlFor="edit-message">
            <input
              value={this.state.message}
              onChange={this.handleInput}
              type="text"
              className="edit-message-input edit-message-modal-form__input"
              id="edit-message"
              ref={this.inputRef}
              autoComplete="off"
            />
          </label>
          <button
            type="submit"
            className="edit-message-button edit-message-modal-form__button"
          >
            Edit
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state: RootStateOrAny) => ({
  isShown: state.chat.editModal,
  editedMessage: state.chat.editedMessage,
});

const mapDispatchToProps = {
  hideEditModal,
  editMessage,
  setEditedMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);
