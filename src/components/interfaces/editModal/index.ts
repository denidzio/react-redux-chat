export type { default as IEditModalProps } from "./IEditModalProps";
export type { default as IEditModalState } from "./IEditModalState";
