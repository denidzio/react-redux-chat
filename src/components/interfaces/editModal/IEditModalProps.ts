import { IMessage } from "../../../interfaces";

interface IEditModalProps {
  isShown: boolean;
  editedMessage: IMessage | null;
  editMessage: (message: string) => void;
  hideEditModal: () => void;
  setEditedMessage: (message: IMessage | null) => void;
}

export default IEditModalProps;
