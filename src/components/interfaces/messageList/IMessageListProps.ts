import { IMessage, IUser } from "../../../interfaces";

interface IMessageListProps {
  messages: IMessage[];
  user: IUser;
  setEditedMessage: (message: IMessage) => void;
  showEditModal: () => void;
}

export default IMessageListProps;
