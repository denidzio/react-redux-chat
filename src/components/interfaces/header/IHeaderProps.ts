import { IMessage } from "../../../interfaces";

interface IHeaderProps {
  messages: IMessage[];
}

export default IHeaderProps;
