import { IMessage } from "../../../interfaces";

interface IMessageProps {
  message: IMessage;
  likeMessage: (messageId: string) => void;
}

export default IMessageProps;
