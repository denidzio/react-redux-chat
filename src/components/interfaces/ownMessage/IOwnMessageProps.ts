import { IMessage } from "../../../interfaces";

interface IOwnMessageProps {
  message: IMessage;
  showEditModal: () => void;
  deleteMessage: (messageId: string) => void;
  setEditedMessage: (message: IMessage) => void;
}

export default IOwnMessageProps;
