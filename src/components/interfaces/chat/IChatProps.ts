import { IMessage } from "../../../interfaces";

interface IChatProps {
  url: string;
  isLoad: boolean;
  hidePreloader: () => void;
  setMessages: (messages: IMessage[]) => void;
}

export default IChatProps;
