interface IMessageInputProps {
  sendMessage: (message: string) => void;
}

export default IMessageInputProps;
