import React, { Component, Fragment, createRef } from "react";
import { connect, RootStateOrAny } from "react-redux";
import { getDiffDays, getDividerText } from "../services/chat.service";

import { IMessage } from "../interfaces";
import { IMessageListProps, IMessageListState } from "./interfaces/messageList";
import { showEditModal, setEditedMessage } from "../actions/chat";
import { Message, OwnMessage } from "./";

class MessageList extends Component<IMessageListProps, IMessageListState> {
  constructor(props: IMessageListProps) {
    super(props);
    this.handleKeyUp = this.handleKeyUp.bind(this);
  }

  listRef = createRef<HTMLDivElement>();

  componentDidMount() {
    window.addEventListener("keyup", this.handleKeyUp);
    window.addEventListener("keydown", this.preventScroll);

    this.scrollToBottom();
  }

  componentDidUpdate(prevProps: IMessageListProps) {
    if (this.props.messages.length > prevProps.messages.length) {
      this.scrollToBottom();
    }
  }

  handleKeyUp(e: KeyboardEvent) {
    if (e.key !== "ArrowUp") {
      return;
    }

    if (!this.props.user) {
      return;
    }

    const lastMessage = this.props.messages
      .filter((message) => message.userId === this.props.user.id)
      .reverse()[0];

    this.props.setEditedMessage(lastMessage);
    this.props.showEditModal();
  }

  preventScroll(e: KeyboardEvent) {
    if (e.key === "ArrowUp") {
      e.preventDefault();
    }
  }

  componentWillUnmount() {
    window.removeEventListener("keyup", this.handleKeyUp);
    window.removeEventListener("keypress", this.preventScroll);
  }

  scrollToBottom() {
    const node = this.listRef.current;

    if (!node) {
      return;
    }

    node.scrollTop = node.scrollHeight;
  }

  renderDivider(currentMsg: IMessage, prevMsg: IMessage) {
    if (prevMsg && getDiffDays(currentMsg.createdAt, prevMsg.createdAt) === 0) {
      return null;
    }

    return (
      <div className="messages-divider">
        {getDividerText(currentMsg.createdAt)}
      </div>
    );
  }

  render() {
    const messages = this.props.messages;

    if (!this.props.user) {
      return null;
    }

    return (
      <div className="message-list" ref={this.listRef}>
        {messages.map((message, index) => (
          <Fragment key={message.id}>
            {this.renderDivider(message, messages[index - 1])}
            {message.userId === this.props.user.id ? (
              <OwnMessage message={message} />
            ) : (
              <Message message={message} />
            )}
          </Fragment>
        ))}
      </div>
    );
  }
}

const mapStateToProps = (state: RootStateOrAny) => ({
  user: state.chat.user,
  messages: state.chat.messages,
});

const mapDispatchToProps = {
  setEditedMessage,
  showEditModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
