import { AnyAction } from "redux";
import actionType from "../../actions/chat/actionType";
import { IChatStore } from "../../interfaces";
import {
  createMessage,
  deleteMessage,
  editMessage,
  likeMessage,
  updateMessage,
} from "../../services/message.service";

const initialState: IChatStore = {
  messages: [],
  editModal: false,
  preloader: true,
  user: {
    id: "533b5230-1b8f-11e8-9629-c7eca82aa7bd",
    name: "Denys",
    avatar: "https://i.ibb.co/vhmLVJQ/ava.jpg",
  },
  editedMessage: null,
};

const reducer = (state = initialState, action: AnyAction) => {
  const type = action.type;

  if (type === actionType.SHOW_EDIT_MODAL) {
    return { ...state, editModal: true };
  }

  if (type === actionType.HIDE_EDIT_MODAL) {
    return { ...state, editModal: false };
  }

  if (type === actionType.SHOW_PRELOADER) {
    return { ...state, preloader: true };
  }

  if (type === actionType.HIDE_PRELOADER) {
    return { ...state, preloader: false };
  }

  if (type === actionType.SET_MESSAGES) {
    if (action.payload) {
      return { ...state, messages: action.payload };
    }
  }

  if (type === actionType.SEND_MESSAGE) {
    if (action.payload !== undefined) {
      return {
        ...state,
        messages: [
          ...state.messages,
          createMessage(state.user, action.payload),
        ],
      };
    }
  }

  if (type === actionType.DELETE_MESSAGE) {
    if (action.payload !== undefined) {
      return {
        ...state,
        messages: deleteMessage(state.messages, action.payload),
      };
    }
  }

  if (type === actionType.SET_EDITED_MESSAGE) {
    if (action.payload !== undefined) {
      return { ...state, editedMessage: action.payload };
    }
  }

  if (type === actionType.EDIT_MESSAGE) {
    if (action.payload !== undefined && state.editedMessage) {
      return {
        ...state,
        messages: updateMessage(
          state.messages,
          editMessage(state.editedMessage, action.payload)
        ),
      };
    }
  }

  if (type === actionType.LIKE_MESSAGE) {
    if (action.payload !== undefined) {
      const likedMessage = likeMessage(state.messages, action.payload);

      if (likedMessage) {
        return {
          ...state,
          messages: updateMessage(state.messages, likedMessage),
        };
      }
    }
  }

  return state;
};

export default reducer;
