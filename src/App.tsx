import React from "react";
import Chat from "./components/Chat";

function App() {
  return (
    <main className="app">
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
    </main>
  );
}

export default App;
