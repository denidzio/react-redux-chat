import { IMessage } from "../../interfaces";
import actionType from "./actionType";

export const showEditModal = () => ({
  type: actionType.SHOW_EDIT_MODAL,
});

export const hideEditModal = () => ({
  type: actionType.HIDE_EDIT_MODAL,
});

export const showPreloader = () => ({
  type: actionType.SHOW_PRELOADER,
});

export const hidePreloader = () => ({
  type: actionType.HIDE_PRELOADER,
});

export const setMessages = (messages: IMessage[]) => ({
  type: actionType.SET_MESSAGES,
  payload: messages,
});

export const sendMessage = (message: string) => ({
  type: actionType.SEND_MESSAGE,
  payload: message,
});

export const deleteMessage = (messageId: string) => ({
  type: actionType.DELETE_MESSAGE,
  payload: messageId,
});

export const setEditedMessage = (message: IMessage | null) => ({
  type: actionType.SET_EDITED_MESSAGE,
  payload: message,
});

export const editMessage = (message: string) => ({
  type: actionType.EDIT_MESSAGE,
  payload: message,
});

export const likeMessage = (messageId: string) => ({
  type: actionType.LIKE_MESSAGE,
  payload: messageId,
});
