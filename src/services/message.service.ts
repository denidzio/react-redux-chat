import { DateTime } from "luxon";
import { generateId } from "../helpers/id.helper";

import { IMessage, IUser } from "../interfaces";

export const createMessage = (user: IUser, text: string): IMessage => {
  return {
    text,
    id: generateId(),
    user: user.name,
    userId: user.id,
    avatar: user.avatar,
    createdAt: DateTime.utc().toString(),
    editedAt: "",
  };
};

export const updateMessage = (messages: IMessage[], message: IMessage) => {
  const updatedMessageId = messages.findIndex((m) => m.id === message.id);

  if (updatedMessageId !== -1) {
    return messages
      .slice(0, updatedMessageId)
      .concat(message, messages.slice(updatedMessageId + 1));
  }

  return messages;
};

export const deleteMessage = (messages: IMessage[], messageId: string) => {
  return messages.filter((message) => message.id !== messageId);
};

export const editMessage = (message: IMessage, text: string) => {
  return { ...message, text, editedAt: new Date().toISOString() };
};

export const likeMessage = (messages: IMessage[], messageId: string) => {
  const message = messages.find((message) => message.id === messageId);

  if (!message) {
    return;
  }

  return { ...message, liked: !message.liked };
};
