import { IMessage } from "../interfaces";
import { DateTime } from "luxon";

export const getNumberOfChatMembers = (messages: IMessage[]) => {
  const uniqUsersId = new Set();

  for (const message of messages) {
    uniqUsersId.add(message.userId);
  }

  return uniqUsersId.size;
};

export const truncateDate = (date: string) => {
  return DateTime.fromISO(date).toUTC().startOf("day");
};

export const getHeaderDate = (date: string) => {
  return DateTime.fromISO(date).toUTC().toFormat("dd.MM.yyyy HH:mm");
};

export const getMessageTime = (date: string) => {
  return DateTime.fromISO(date).toUTC().toFormat("HH:mm");
};

export const getDiffDays = (date_1: string, date_2: string) => {
  const moment_1 = truncateDate(date_1);
  const moment_2 = truncateDate(date_2);

  return Math.abs(moment_1.diff(moment_2, "days").as("days"));
};

export const getDividerText = (messageDate: string) => {
  const diffDays = getDiffDays(DateTime.utc().toString(), messageDate);

  if (diffDays === 0) {
    return "Today";
  }

  if (diffDays === 1) {
    return "Yesterday";
  }

  return DateTime.fromISO(messageDate)
    .setLocale("en-gb")
    .toFormat("DDDD")
    .split(" ")
    .slice(0, 3)
    .join(" ");
};
