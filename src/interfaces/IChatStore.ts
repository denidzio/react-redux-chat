import IMessage from "./IMessage";
import IUser from "./IUser";

interface IChatStore {
  messages: IMessage[];
  editModal: boolean;
  preloader: boolean;
  user: IUser;
  editedMessage: IMessage | null;
}

export default IChatStore;
