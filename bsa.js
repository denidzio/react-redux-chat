import Chat from "./src/components/Chat";
import rootReducer from "./src/reducers/";

const defaultObject = {
    Chat,
    rootReducer
};

export default defaultObject;
